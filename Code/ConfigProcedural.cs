using System.Text;

namespace RegexpTest {
    public class ConfigProcedural {

        private readonly byte[] _config;



        public ConfigProcedural(string configText) {
            _config = Encoding.ASCII.GetBytes(configText);
        }

        public string GetValue(string key) {
            string value = null;
            
            bool isSkipSpaces = true;
            bool isSkipLine = false;

            int keyCharIndex = 0;
            bool isKeyChecking = false;
            bool isKeyFound = false;
            
            bool isAssignmentSearch = false;
            bool isValueReading = false;
            
            for (int i = 0; i < _config.Length; i++) {
                char c = (char)_config[i];
                
                // get start character of key or value (when 'isKeyFound' is set)
                if (isSkipSpaces) {
                    if (IsLetter(c)) {
                        isSkipSpaces = false;
                        
                        if (isKeyFound) {
                            isValueReading = true;
                        } else {
                            isKeyChecking = true;
                            keyCharIndex = 0;
                        }
                    } else
                        continue;
                }
                
                if (isSkipLine) {
                    if (c == '\n') {
                        isSkipLine = false;
                        isSkipSpaces = true;
                    }
                    continue;
                }
                
                if (isKeyChecking) {
                    if (keyCharIndex < key.Length && key[keyCharIndex] == c) {
                        keyCharIndex++;
                        
                        continue;
                    }
                    
                    isKeyChecking = false;
                    
                    if (keyCharIndex == key.Length) {
                        value = "";
                        isKeyFound = true;
                        isAssignmentSearch = true;
                    } else {
                        isSkipLine = true;
                    }
                }
                
                if (isAssignmentSearch && c == '=') {
                    isAssignmentSearch = false;
                    isSkipSpaces = true;
                }
                
                if (isValueReading) {
                    if (IsLetter(c)) {
                        value += c;
                    } else
                        break;
                }
            }
            
            return value;
        }

        private bool IsLetter(char c) {
            return (c >= 'a' && c <= 'z')
                || (c >= 'A' && c <= 'Z')
                || (c >= '0' && c <= '9')
                || c == '_' || c == '.';
        }

    }
}