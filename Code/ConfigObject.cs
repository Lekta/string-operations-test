using System.Linq;

namespace RegexpTest {
    public class ConfigObject {

        private readonly Param[] _params; 



        public ConfigObject(string configText) {
            var paramsArray = configText.Split('\n');
            
            _params = new Param[paramsArray.Length];
            for (int i = 0; i < paramsArray.Length; i++) {
                _params[i] = new Param(paramsArray[i]);
            }
        }

        public string GetValue(string key) {
            return _params.FirstOrDefault(p => p.Key == key)?.Value;
        }


        private class Param {

            public readonly string Key;
            public readonly string Value;


            public Param(string paramText) {
                var keyValue = paramText.Split('=');
                
                Key = keyValue[0].Trim();
                Value = (keyValue.Length < 2 ? null : keyValue[1])?.Trim();
            }

        }
        
    }
}