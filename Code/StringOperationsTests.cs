using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace RegexpTest {
    public class StringOperationsTests {

        private delegate string GetValueOperator(string config, string key);

        private ConfigObject _configOOP;
        private ConfigProcedural _configProcedural;



        public void RunTests() {
            Console.WriteLine("\nRun strings operations tests...");
            
            var config = Data.GetConfig();
            
            Console.Write("RegEx.       ");
            TestMethod(config, GetConfigValue_RegEx);
            
            Console.Write("Split.       ");
            TestMethod(config, GetConfigValue_Split);
            
            Console.Write("OOP.         ");
            TestMethod(config, GetConfigValue_ObjectOriented);
            
            Console.Write("Procedural.  ");
            TestMethod(config, GetConfigValue_Procedural);
            
            Console.Write("Idle.        ");
            TestMethod(config, ReturnKeyBack);
            
            Console.Write("Allocate 13 bytes. ");
            TestMethod(config, Allocate13bytes);
            
            Console.WriteLine("\nTests finished.");
        }

        private void TestMethod(string config, GetValueOperator getValue) {
            int loops = 10000;
            
            var memBytes1st = BytesUsedByMethod(() => getValue(config, Data.KeyA));
            var memBytes2nd = BytesAverageUsedByMethod(() => getValue(config, Data.KeyA));
            
            var validationMsg = ValidateValues(config, getValue);
            int duration = MsUsedByMethodLoop(config, loops, getValue);
            
            Console.WriteLine("Memory allocated, 1st pass: "+ memBytes1st +" bytes, 2nd (avg) pass: "+ memBytes2nd +" bytes;  duration (by "+ loops +" loops) - "+ duration +" ms.  Validation "+ validationMsg);
        }

        private string ValidateValues(string config, GetValueOperator getValue) {
            string receivedA = getValue(config, Data.KeyA);
            string receivedB = getValue(config, Data.KeyB);
            string receivedC = getValue(config, Data.KeyC);
            
            bool isValidA = receivedA == Data.ValueA;
            bool isValidB = receivedB == Data.ValueB;
            bool isValidC = receivedC == Data.ValueC;
            
            return (isValidA && isValidB && isValidC)
                ? "ok"
                : "failed, received values: "+ receivedA +", "+ receivedB +", "+ receivedC;
        }

        private int BytesUsedByMethod(Action method) {
            var memBefore = GC.GetTotalMemory(true);
            
            method.Invoke();
            
            long memUsed = GC.GetTotalMemory(false) - memBefore;
            return (int)memUsed;
        }

        private int BytesAverageUsedByMethod(Action method) {
            var memBefore = GC.GetTotalMemory(true);

            int chunkSize = 8192; // TODO: get real chunk size from system 
            for (int i = 0; i < chunkSize; i++) {
                method.Invoke();
            }
            long memUsed = GC.GetTotalMemory(false) - memBefore;
            
            return (int)memUsed / chunkSize;
        }

        private int MsUsedByMethodLoop(string config, int loopsCnt, GetValueOperator getValue) {
            var watch = Stopwatch.StartNew();
            
            int calcHash = 0;
            for (int i = 0; i < loopsCnt; i++) {
                string key = Data.GetRandomKey();
                string value = getValue(config, key);
                
                calcHash += value.GetHashCode();
            }
            
            return (int)watch.ElapsedMilliseconds;
        }


        private string GetConfigValue_RegEx(string config, string key) {
            string regexPattern = "^"+ key +"\\s*=?\\s*(\\S*)";
            
            var reg = new Regex(regexPattern, RegexOptions.Multiline);
            var match = reg.Match(config);
            
            return match.Groups[1].Value;
        }

        private string GetConfigValue_Split(string config, string key) {
            var parameters = config.Split('\n');
            
            string matchedPar = parameters.FirstOrDefault(p => p.Trim().StartsWith(key));
            if (matchedPar == null)
                return null;
            
            var pKeyValue = matchedPar.Split('=');
            bool hasValuePart = pKeyValue.Length >= 2;
            
            return hasValuePart ? pKeyValue[1].Trim() : null;
        }

        private string GetConfigValue_ObjectOriented(string config, string key) {
            if (_configOOP == null) {
                _configOOP = new ConfigObject(config);
            }
            return _configOOP.GetValue(key);
        }

        private string GetConfigValue_Procedural(string config, string key) {
            if (_configProcedural == null) {
                _configProcedural = new ConfigProcedural(config);
            }
            return _configProcedural.GetValue(key);
        }

        private string ReturnKeyBack(string config, string key) {
            return key;
        }

        private string Allocate13bytes(string config, string key) {
            var bytes = new Use13bytes();
            return String.Empty;
        }

        
        private class Use13bytes {
            public Int32 a, b, c;
            public Byte d;
        }

    }
}