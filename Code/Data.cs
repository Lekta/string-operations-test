using System;

namespace RegexpTest {
    public static class Data {

        public static string KeyA = "keyA";
        public static string KeyB = "keyB";
        public static string KeyC = "keyC";
        
        public static string ValueA = "1000";
        public static string ValueB = "847397245";
        public static string ValueC = "42";
        
        private static Random _random = new Random();



        public static string GetConfig() {
            return @"
adg = 1
bfdfdg = 1
cdgfg = 1
dd55g = 1
"+ KeyA +" = "+ ValueA +@"
fdregg = 1
fdhghrthg = 1
fdgbg = 1
fgbfbdg = 1
fbfgdg = 1
fdgg = 1
fbbfgbddg = 1
fdttg = 1
fbgbfgbdg = 1
fgbfdg = 1
fdfgg = 1
fgbgbgdg = 1
"+ KeyB +" = "+ ValueB +@"
fdgghj = 1
fdhvmjfg = 1
fdgfhnghg = 1
fkbikntdg = 1
fkuydg = 1
fdbkytkbyg = 1
fdhbjg = 1
"+ KeyC +" = "+ ValueC +@"
fdkbygkg = 1
fbghjgfdg = 1
fdkbghg = 1
fghbjdg = 1
fdkbykg = 1
fgbukdg = 1
fdhgbjkg = 1";
        }

        public static string GetRandomKey() {
            var keys = new[] {
                KeyA,
                KeyB,
                KeyC
            };
            return keys[_random.Next(keys.Length)];
        }
        
    }
}